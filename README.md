# Digitale Freiheit

## Nextcloud

- [Wölkli](https://cloud.woelkli.com/apps/dashboard/)


## Tests & Tools

- [**Cover Your Tracks**](https://coveryourtracks.eff.org/): Testen Sie Ihren Browser, um zu sehen, wie gut Sie vor Tracking und Fingerprinting geschützt sind:
